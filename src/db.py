import csv
import os.path

FILENAME = "data.csv"


def flatten(dictionary, parent_key="", sep="_"):
    flattened_data = {}
    for k, v in dictionary.items():
        new_key = parent_key + sep + k if parent_key else k
        if isinstance(v, dict):
            flattened_data.update(flatten(v, new_key, sep=sep))
        else:
            flattened_data[new_key] = v
    return flattened_data


def append_to_csv(filename, data):
    exists = os.path.exists(filename)
    result = [flatten(d) for d in data]
    with open(filename, "a", newline="") as file:
        writer = csv.DictWriter(file, fieldnames=result[0].keys())
        if not exists:
            writer.writeheader()
        writer.writerows(result)

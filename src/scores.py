import json


class ScytheScoreError(Exception):
    pass


class Config:
    def __init__(self, datetime, player, faction, board):
        self.datetime = datetime
        self.player = player
        self.faction = faction
        self.board = board


class Input:
    def __init__(
        self,
        coins_in_hand,
        stars_placed,
        territories_controlled,
        factory_bonus,
        resources_controlled,
        structure_bonuses_achieved,
        popularity,
        automa=False,
    ):
        self.coins_in_hand = coins_in_hand
        self.stars_placed = stars_placed
        self.territories_controlled = territories_controlled
        self.factory_bonus = factory_bonus
        self.resources_controlled = resources_controlled
        self.structure_bonuses_achieved = structure_bonuses_achieved
        self.popularity = popularity
        self.automa = automa


class ScytheScoreCalculator:
    def __init__(
        self,
        datetime,
        player,
        faction,
        board,
        coins_in_hand,
        stars_placed,
        territories_controlled,
        factory_bonus,
        resources_controlled,
        structure_bonuses_achieved,
        popularity,
        automa=False,
    ):
        self.config = Config(datetime=datetime, player=player, faction=faction, board=board)
        self.input = Input(
            coins_in_hand,
            stars_placed,
            territories_controlled,
            factory_bonus,
            resources_controlled,
            structure_bonuses_achieved,
            popularity,
            automa,
        )

    @property
    def coef(self):
        return calculate_coef(popularity=self.input.popularity, automa=self.input.automa)

    def details(self, pprint=False):
        result = {
            "config": self.config.__dict__,
            "input": self.input.__dict__,
            "coef": {**self.coef},
            "score": {
                **{
                    "stars score": self.calculate_coins_from_stars(),
                    "territories score": self.calculate_coins_from_territories(),
                    "resources score": self.calculate_coins_from_resources(),
                    "structure score": self.calculate_structure_bonus(),
                }
            },
            "total": self.calculate_score(),
        }
        if pprint:
            print(json.dumps(result, indent=2, ensure_ascii=False))
        return result

    def calculate_score(self):
        coins = (
            self.calculate_coins_from_stars()
            + self.calculate_coins_from_territories()
            + self.calculate_coins_from_resources()
            + self.calculate_structure_bonus()
        )
        coins += self.input.coins_in_hand  # Coins accumulated during the game
        return coins

    def calculate_coins_from_stars(self):
        return self.coef["stars"] * self.input.stars_placed

    def calculate_coins_from_territories(self):
        factory_bonus = 2 if self.input.factory_bonus else 0
        total_territories = (self.input.territories_controlled + factory_bonus) * self.coef["territories"]
        return total_territories

    def calculate_coins_from_resources(self):
        if self.input.automa:
            return 0
        if self.input.resources_controlled % 2 == 0:
            return self.coef["resources"] * self.input.resources_controlled // 2
        else:
            return self.coef["resources"] * (self.input.resources_controlled - 1) // 2

    def calculate_structure_bonus(self):
        return self.input.structure_bonuses_achieved


def calculate_coef(popularity, automa):
    if automa:
        return {"stars": 4, "territories": 3, "resources": 0}
    if popularity < 0 or popularity > 18:
        raise ScytheScoreError("Popularity should be between 1 and 18")
    elif popularity <= 6:
        return {"stars": 3, "territories": 2, "resources": 1}
    elif 7 <= popularity <= 12:
        return {"stars": 4, "territories": 3, "resources": 2}
    elif popularity >= 13:
        return {"stars": 5, "territories": 4, "resources": 3}

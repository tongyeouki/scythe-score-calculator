# Scythe Score Calculator

Custom score counter for the dedicated board game. 

## Usage

```
$ cp .app.sample.py app.py
$ python app.py
```
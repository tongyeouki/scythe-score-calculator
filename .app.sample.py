from src.db import append_to_csv, FILENAME
from src.scores import ScytheScoreCalculator


SESSION = "2024-01-21 12:00:00"


human = ScytheScoreCalculator(
    datetime=SESSION,
    player="John Doe",
    faction="Rusviet",
    board="Mechanical",
    coins_in_hand=18,
    stars_placed=6,
    territories_controlled=9,
    factory_bonus=True,
    resources_controlled=7,
    structure_bonuses_achieved=9,
    popularity=13,
)
human.details(True)


automa = ScytheScoreCalculator(
    datetime=SESSION,
    faction="Polonia",
    board="N/A",
    coins_in_hand=14,
    stars_placed=6,
    territories_controlled=5,
    factory_bonus=False,
    resources_controlled=0,
    structure_bonuses_achieved=4,
    popularity=10,
    automa=True,
    player="Autometta"
)
automa.details(True)


append_to_csv(FILENAME, [human.details(), automa.details()])
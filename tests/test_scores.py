"""0-6 : Etoiles: 3 ; territories: 2 ; resources: 1"""
import pytest

from src.scores import calculate_coef, ScytheScoreError, ScytheScoreCalculator


@pytest.fixture
def fixture():
    score = ScytheScoreCalculator(
        player="Test",
        faction="Rusviet",
        board="Industriel",
        coins_in_hand=10,
        stars_placed=6,
        territories_controlled=5,
        factory_bonus=False,
        resources_controlled=10,
        structure_bonuses_achieved=5,
        popularity=10,
    )
    return score


def test_popularity_level_at_six_should_return_coef_one():
    # Arrange
    popularity = 6
    # Act
    coef = calculate_coef(popularity=popularity, automa=False)
    # Assert
    assert coef == {"stars": 3, "territories": 2, "resources": 1}


def test_popularity_level_below_zero_should_raise_exception():
    # Arrange
    popularity = -2
    # Act & Assert
    with pytest.raises(ScytheScoreError):
        calculate_coef(popularity, automa=False)


def test_popularity_level_at_seven_should_return_coef_two():
    # Arrange
    popularity = 7
    # Act
    coef = calculate_coef(popularity=popularity, automa=False)
    # Assert
    assert coef == {"stars": 4, "territories": 3, "resources": 2}


def test_popularity_level_at_twelve_should_return_coef_two():
    # Arrange
    popularity = 12
    # Act
    coef = calculate_coef(popularity=popularity, automa=False)
    # Assert
    assert coef == {"stars": 4, "territories": 3, "resources": 2}


def test_popularity_level_at_thirteen_should_return_coef_three():
    # Arrange
    popularity = 13
    # Act
    coef = calculate_coef(popularity=popularity, automa=False)
    # Assert
    assert coef == {"stars": 5, "territories": 4, "resources": 3}


def test_popularity_level_at_eighteen_should_return_coef_three():
    # Arrange
    popularity = 18
    # Act
    coef = calculate_coef(popularity=popularity, automa=False)
    # Assert
    assert coef == {"stars": 5, "territories": 4, "resources": 3}


def test_impair_resources(fixture):
    # Arrange
    fixture.resources_controlled = 7
    # Act
    result = fixture.calculate_coins_from_resources()
    # Assert
    assert result == 12
